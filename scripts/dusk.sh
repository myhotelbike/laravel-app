#!/bin/sh

wait-for.sh web:80 || { echo "Could not reach NGINX in time"; exit 1; }

su-exec www-data php artisan dusk "$@"
