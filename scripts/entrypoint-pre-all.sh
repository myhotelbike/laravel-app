#!/bin/sh

echo "xdebug.client_host = $(/sbin/ip route|awk '/default/ { print $3 }')" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

sed -i "s/{UPLOAD_MAX_FILESIZE}/$UPLOAD_MAX_FILESIZE/" /usr/local/etc/php-fpm.d/www.conf
sed -i "s/{POST_MAX_SIZE}/$POST_MAX_SIZE/" /usr/local/etc/php-fpm.d/www.conf

if [ -d "/secrets/.postgresql" ]
then
  mkdir /home/www-data/.postgresql
  cp -L /secrets/.postgresql/* /home/www-data/.postgresql/
  chmod -R 400 /home/www-data/.postgresql/*
  chown -R www-data:www-data /home/www-data/.postgresql
fi

if [ -n "$ES_HOST" ] && [ -n "$ES_PORT" ]
then
  /usr/bin/wait-for.sh "$ES_HOST:$ES_PORT" || { echo "Could not reach Elastic Search in time on $ES_HOST:$ES_PORT"; exit 1; }
fi

if [ -n "$REDIS_HOST" ] && [ -n "$REDIS_PORT" ]
then
  /usr/bin/wait-for.sh "$REDIS_HOST:$REDIS_PORT" || { echo "Could not reach Redis in time on $REDIS_HOST:$REDIS_PORT"; exit 1; }
fi

if [ -n "$DB_HOST" ] && [ -n "$DB_PORT" ]
then
  /usr/bin/wait-for.sh "$DB_HOST:$DB_PORT" || { echo "Could not reach Database in time on $DB_HOST:$DB_PORT"; exit 1; }
fi

if [ -n "$VARNISH_HOST" ] && [ -n "$VARNISH_PORT" ]
then
  /usr/bin/wait-for.sh "$VARNISH_HOST:$VARNISH_PORT" || { echo "Could not reach Varnish in time on $VARNISH_HOST:$VARNISH_PORT"; exit 1; }
fi
