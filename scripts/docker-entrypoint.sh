#!/bin/sh

if [ -f ".env.$APP_ENV" ]
then
  set -o allexport
  . ".env.$APP_ENV"
  set +o allexport
fi

/usr/bin/entrypoint-pre-all.sh || exit 1;

if [ -f "/usr/bin/local-cron.sh" ]
then
  { while : ; do sleep 1m; /usr/bin/local-cron.sh; done } > /dev/null 2>&1 &
fi

if [ -n "$1" ]
then
  su-exec www-data php artisan deploy console || exit 1;

  exec "$@"
  exit $?
fi

su-exec www-data php artisan deploy server || exit 1;

exec /usr/local/sbin/php-fpm
