#!/bin/sh

su-exec www-data /usr/local/bin/php -d memory_limit=-1 vendor/bin/phpunit "$@"
