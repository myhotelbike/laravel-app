# Build
## Production
```shell
docker build -f Dockerfile.8.3 -t registry.gitlab.com/myhotelbike/laravel-app/8.3/prod:1.6.0 .
```
## Local
```shell
docker build -f Dockerfile.8.3 -t registry.gitlab.com/myhotelbike/laravel-app/8.3/local:1.6.0 --build-arg="XDEBUG=1" .
```
